# Go+Docker on AWS project sample

This project sample shows the usage of _to be continuous_ templates:

* Go
* SonarQube (from [sonarcloud.io](https://sonarcloud.io/))
* Docker
* AWS (Amazon Web Services)
* Postman

The project deploys a basic API developped in Go, packaged as a Docker image on AWS ECS (Fargate), and implements automated acceptance tests with Postman.

## Go template features

This project uses the default Go template configuration.

The Go template natively implements [unit tests report](https://docs.gitlab.com/ee/ci/testing/unit_test_reports.html) and
[code coverage](https://docs.gitlab.com/ee/ci/testing/code_coverage.html) integration in GitLab.

The Go template also enforces:

* [test report integration](https://docs.gitlab.com/ee/ci/testing/unit_test_reports.html),
* and [code coverage integration](https://docs.gitlab.com/ee/user/project/pipelines/settings.html#test-coverage-report-badge).

## SonarQube template features

This project uses the following features from the SonarQube template:

* Defines the `SONAR_HOST_URL` (SonarQube server host),
* Defines `organization` & `projectKey` from [sonarcloud.io](https://sonarcloud.io/) in `sonar-project.properties`,
* Defines :lock: `$SONAR_TOKEN` as secret CI/CD variable,
* Uses the `sonar-project.properties` to specify project specific configuration:
    * source and test folders,
    * code coverage report,
    * unit test reports.

## Docker template features

This project builds and _pushes_ a Docker image embedding the built application.
For this, a Docker registry is required.
As a matter of fact there are two options (this project implements the first one).

### Option 1: use the GitLab registry

This is the easiest as the Docker template is preconfigured to work this way.

Extra requirements:

1. make sure AWS has (network) access to your GitLab registry (true in our case through the internet),
2. (if your registry is non-public) create a secret in AWS Secret Manager with your Docker registry credentials,
3. (if your registry is non-public) declare those credentials in your ECS task definition.

[More info](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/private-auth.html).

### Option 2: use the AWS registry (ECR)

This project could alternatively use the AWS [Elastic Container Registry](https://docs.aws.amazon.com/AmazonECR/latest/userguide/) 
(ECR) to push its Docker images.

Nevertheless it's not easy as AWS ECR doesn't deliver any persistent credentials, but temporary credentials instead (can be retrieved with `aws CLI`), making more difficult to integrate with the _to be continuous_ Docker template.

## AWS template features

This project uses AWS [Elastic Container Service](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/) 
(ECS) to build and deploy this application on AWS.

This project uses the following features from the AWS template:

* Enables review, staging and production environments (by declaring the `$AWS_REVIEW_ENABLED`, `$AWS_STAGING_ENABLED` and `$AWS_PROD_ENABLED` in the project variables); the AWS template implements environments integration and review environment cleanup support (manually or when the related development branch is deleted).
* Configures AWS authentication by specifying AWS credentials (`$AWS_ACCESS_KEY_ID`, `$AWS_SECRET_ACCESS_KEY` and `$AWS_DEFAULT_REGION`) as (secret) CI/CD project variables.

In order to perform AWS deployments, the project implements:

* `aws-deploy.sh` script: generic deployment script using `aws ecs` commands,
* `aws-cleanup.sh` script: generic cleanup script using the `aws ecs` command.

Both scripts make use of variables dynamically evaluated and exposed by the templates:

* `${docker_image_digest}`: the Docker image (with digest)built upstream. This is propagated by the Docker template as a [dotenv artifact](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreportsdotenv). This variable is inserted in the ECS task definition JSON file.
* `${environment_name}`: the application target name to use in this environment (ex: `myproject-review-fix-bug-12` or `myproject-staging`); 
  this is used as the SAM/CloudFormation [stack name](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/sam-cli-command-reference-sam-deploy.html),
* `${environment_type}`: the environment type (`review`, `integration`, `staging` or `production`); added as a tag.

Lastly, the deployment script implements the [dynamic way](https://docs.gitlab.com/ee/ci/environments/#set-a-dynamic-environment-url) of
defining the environment URLs: retrieves the generated server URL using [AWS CLI `--query` option](https://docs.aws.amazon.com/cli/latest/userguide/cli-usage-filter.html#cli-usage-filter-client-side), and dumps it into a `environment_url.txt` file, supported by the template.

## Postman

This project also implements Postman acceptance tests, simply storing test collections in the default [postman/](./postman) directory.

The upstream deployed environment base url is simply referenced in the Postman tests by using the [{{base_url}} variable](https://learning.postman.com/docs/sending-requests/variables/) evaluated by the template.
