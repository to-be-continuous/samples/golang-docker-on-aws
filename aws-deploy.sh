#!/usr/bin/env bash

set -e # fail on error

function awkenvsubst() {
  awk '{while(match($0,"[$]{[^}]*}")) {var=substr($0,RSTART+2,RLENGTH-3);gsub("[$]{"var"}",ENVIRON[var])}}1'
}
  
# initialize input variables (provided by to-be-continuous templates)
export environment_name=${environment_name:-burger-maker-dev}
export docker_image=${docker_image_digest:-httpd:2.4}

echo -e "[aws-deploy] Deploy \\e[33;1m${environment_name}\\e[0m (image \\e[33;1m${docker_image}\\e[0m)..."

# disable AWS CLI pager
export AWS_PAGER=""

# How to manage multi-env with ECS?
# 1) with FARGATE
# each service can be assigned its own public IP, so all envs can fit in the same cluster
# 2) with ECS
# the simplest idea is to have one cluster per env ? (no review env; only 'staging' and 'production')
# works even for a microservices app: each service is exposed though a dedicated port

# 1: register a new version of the task definition
# first generate task definition with actual Docker image
awkenvsubst < task-def.json > task-def-img.json
aws ecs register-task-definition --family "$environment_name" --cli-input-json file://task-def-img.json

# 2: create/update service
service_arn=$(aws ecs describe-services --services "$environment_name" --query 'services[0].serviceArn' --output text)
if [[ "$service_arn" == "None" ]]
then
  echo -e "Service doesn't exist: create..."

  # retrieve ECS subnets and security groups from tag 'Name: ECS*'
  subnet_ids=$(aws ec2 describe-subnets --filters "Name=tag:Name,Values=ECS*" --query 'Subnets[*].SubnetId' --output text | sed -e 's/\t/,/g')
  sec_group_ids=$(aws ec2 describe-security-groups --filters "Name=tag:Name,Values=ECS*" --query 'SecurityGroups[*].GroupId' --output text | sed -e 's/\t/,/g')

  prev_task_arn=None

  aws ecs create-service \
    --service-name "$environment_name" \
    --task-definition "$environment_name" \
    --desired-count 1 \
    --launch-type FARGATE \
    --network-configuration "awsvpcConfiguration={subnets=[${subnet_ids}],securityGroups=[${sec_group_ids}],assignPublicIp=ENABLED}"
else
  echo -e "Service exists: update and force deploy..."
  prev_task_arn=$(aws ecs list-tasks --service-name "$environment_name" --query 'taskArns[0]' --output text)
  aws ecs update-service --service "$environment_name" --task-definition "$environment_name" --force-new-deployment
fi

aws ecs list-tasks --service-name "$environment_name"

# 3: loop until new task is running
echo -e "Waiting for new task to start..."
for i in $(seq 1 20)
do
  task_arn=$(aws ecs list-tasks --service-name "$environment_name" --query 'taskArns[0]' --output text)
  if [[ "$task_arn" != "$prev_task_arn" ]]
  then
    break
  fi
  sleep 6
done

if [[ "$task_arn" == "$prev_task_arn" ]]
then
  echo -e "[ERROR] Failed waiting for new task to start"
  exit 1
fi

aws ecs wait tasks-running --tasks "$task_arn"
echo -e "New task running: \\e[33;1m${task_arn}\\e[0m"

# retrieve attached network interface ID
eni_id=$(aws ecs describe-tasks --tasks "$task_arn" --query 'tasks[0].attachments[0].details[?name==`networkInterfaceId`].value' --output text)

# retrieve public DNS from network interface
public_dns=$(aws ec2 describe-network-interfaces --network-interface-ids "$eni_id" --query 'NetworkInterfaces[0].Association.PublicDnsName' --output text)

# Finally set the dynamically generated WebServer Url
echo "http://$public_dns" > environment_url.txt
