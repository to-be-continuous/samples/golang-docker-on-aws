#!/usr/bin/env bash
set -e

# initialize input variables (provided by to-be-continuous templates)
export environment_name=${environment_name:-burger-maker-dev}

echo -e "[aws-cleanup] Cleanup \\e[33;1m${environment_name}\\e[0m..."

# disable AWS CLI pager
export AWS_PAGER=""

# 1: delete service
aws ecs delete-service --service "$environment_name" --force

# 2: deregister task def
# TODO: deregister all revision
