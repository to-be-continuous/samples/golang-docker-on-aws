module to-be-continuous/burger-maker

go 1.22

toolchain go1.24.0

require (
	github.com/google/uuid v1.6.0
	google.golang.org/grpc v1.70.0
)

require golang.org/x/tools/gopls v0.6.9 // indirect
