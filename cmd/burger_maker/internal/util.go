package internal

import (
	"log"
	"os"
	"strconv"
)

// Environment String
type EnvStr string

func (env EnvStr) Or(def string) string {
	if v, ok := os.LookupEnv(string(env)); ok {
		return v
	}

	return def
}

func (env EnvStr) OrDie() string {
	v, ok := os.LookupEnv(string(env))
	if !ok {
		log.Fatalf("missing mandatory environment variable: %s", env)
	}

	return v
}

// Environment Int
type EnvInt string

func (env EnvInt) Or(def int) int {
	if v, ok := os.LookupEnv(string(env)); ok {
		if i, err := strconv.ParseInt(v, 10, 32); err == nil {
			return int(i)
		}

		return def
	}

	return def
}

func (env EnvInt) OrDie() int {
	v, ok := os.LookupEnv(string(env))
	if !ok {
		log.Fatalf("missing mandatory environment variable: %s", env)
	}

	i, err := strconv.ParseInt(v, 10, 32)
	if err != nil {
		log.Fatalf("unable to convert '%s' mandatory environment variable value '%s' to int: %s", env, v, err)
	}

	return int(i)
}

// Environment Bool
type EnvBool string

func (env EnvBool) Or(def bool) bool {
	if v, ok := os.LookupEnv(string(env)); ok {
		if b, err := strconv.ParseBool(v); err == nil {
			return b
		}

		return def
	}

	return def
}

func (env EnvBool) OrDie() bool {
	v, ok := os.LookupEnv(string(env))
	if !ok {
		log.Fatalf("missing mandatory environment variable: %s", env)
	}

	b, err := strconv.ParseBool(v)
	if err != nil {
		log.Fatalf("unable to convert '%s' mandatory environment variable value '%s' to bool: %s", env, v, err)
	}

	return b
}
