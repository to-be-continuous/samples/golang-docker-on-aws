package internal

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"strings"

	"github.com/google/uuid"
)

type StatusError struct {
	Code    int
	Message string
	Err     error
}

func (se StatusError) Error() string {
	if se.Err != nil {
		return se.Message + " > " + se.Err.Error()
	}
	return se.Message
}

func (se StatusError) Status() int {
	if se.Code == 0 {
		if serr, ok := se.Err.(StatusError); ok {
			// if parent error is of type StatusError: inherit
			return serr.Code
		} else {
			// else: internal error
			return http.StatusInternalServerError
		}
	} else {
		return se.Code
	}
}

type ErrorResponse struct {
	Errors []string `json:"errors"`
}

func (er ErrorResponse) Error() string {
	return strings.Join(er.Errors, ", ")
}

var BURGER_RECIPES = map[string][]string{
	"cheeseburger": {"bun", "red onion", "tomato", "ketchup", "salad", "cheddar", "steak"},
	"bacon":        {"bun", "bacon", "garlic sauce", "brie", "steak", "tomato"},
	"farmer":       {"bun", "red onion", "bbq sauce", "stilton", "chicken", "tomato"},
	"fish":         {"bun", "tartare sauce", "cucumber", "old gouda", "fish fillet"},
	"veggie":       {"bun", "goat cheese", "tomato confit", "red onion", "super-secret veggie steak"},
	"frenchie":     {"baguette", "butter", "brie", "smoked ham"},
}

type Burger struct {
	Name        string       `json:"name"`
	Id          string       `json:"id"`
	Ingredients []Ingredient `json:"ingredients"`
}

type Ingredient struct {
	Name string `json:"name"`
	Id   string `json:"id"`
}

/**
 * Returns the list of recipe names
 * GET /api/recipes
 */
func GetRecipes(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "application/json")

	if strings.ToUpper(request.Method) != "GET" {
		http.Error(writer, "Method not allowed", http.StatusMethodNotAllowed)
	} else {
		recipes := make([]string, 0, len(BURGER_RECIPES))
		for name := range BURGER_RECIPES {
			recipes = append(recipes, name)
		}
		writer.WriteHeader(http.StatusOK)
		body, _ := json.Marshal(recipes)
		// nolint
		writer.Write(body)
	}
}

/**
 * Prepares and delivers a burger from the required recipe
 * DELETE /api/burger/{recipe}
 */
func GimeABurger(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "application/json")

	if strings.ToUpper(request.Method) != "DELETE" {
		http.Error(writer, "Method not allowed", http.StatusMethodNotAllowed)
	} else {
		recipe := request.URL.Path[13:]

		if recipe == "any" {
			// pick a recipe at random
			// nolint
			idx := rand.Intn(len(BURGER_RECIPES))
			i := 0
			for name := range BURGER_RECIPES {
				if i == idx {
					recipe = name
					break
				}
				i++
			}
		}

		// check requested recipe exists
		if ingredient_names, ok := BURGER_RECIPES[recipe]; ok {
			var ingredients []Ingredient
			for _, name := range ingredient_names {
				ingredients = append(ingredients, Ingredient{Name: name, Id: uuid.New().String()})
			}
			burger := Burger{
				Name:        recipe,
				Id:          uuid.New().String(),
				Ingredients: ingredients,
			}
			writer.WriteHeader(http.StatusOK)
			body, _ := json.Marshal(burger)
			// nolint
			writer.Write(body)
		} else {
			http.Error(writer, fmt.Sprintf("No such recipe: %s", recipe), http.StatusNotFound)
		}
	}
}
