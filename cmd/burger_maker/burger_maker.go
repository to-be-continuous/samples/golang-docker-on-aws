package main

import (
	"fmt"
	"log"
	"net/http"

	. "to-be-continuous/burger-maker/cmd/burger_maker/internal"
)

func main() {
	port := EnvInt("PORT").Or(8080)
	log.Printf("Launching service on port %d\n", port)

	// health endpoint
	http.HandleFunc("/health", Health)

	// API recipes endpoint
	http.HandleFunc("/api/recipes", GetRecipes)

	// API burger endpoint
	http.HandleFunc("/api/burgers/", GimeABurger)

	// serve static resources
	fs := http.FileServer(http.Dir("./static"))
	http.Handle("/", fs)

	// nolint
	http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
}
